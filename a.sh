sudo docker build . -t analiza                          # Izgradnja kontejnera zDockerfile-om v trenutnega direktorija z ustrezno značko glede na nalogo
sudo docker run -ti -v `pwd`/share:/mnt/share analiza   # Zaženjevanje z preneseno mapo s projektnimi datotekami v TTY mode interaktivno ter sliko
cat /etc/lsb-release                                    # Preverba informacij o operacijskega sistema
pylint --version                                        # Prikaz verzije python linterja - a je že nameščen?
valgrind --version                                      # =||= za profilirno orodje
ls /mnt/share                                           # A so projektni datoteki preneseni od glavnega os-a??
