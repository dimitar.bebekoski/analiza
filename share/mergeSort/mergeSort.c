#include <stdio.h>
#include <stdlib.h>
#include <sys/mman.h>
#include <fcntl.h>
#include <pthread.h>
#include <unistd.h>
#include <wait.h>
#include <semaphore.h>

//static int N 2;

void printHelp(int argc, char** argv);
void submergeSortSimple(int* array, int min1, int max1, int min2, int max2);
void submergeSortProc(int* array, int min1, int max1, int min2, int max2);
void submergeSortThread(int* array, int min1, int max1, int min2, int max2);
void mergeSort(int *array, int min, int max, void(*submergeSort)(int *, int, int, int, int));
void merge(int *arr,int min,int mid,int max);

void submergeSortSimple(int* array, int min1, int max1, int min2, int max2){
	mergeSort(array, min1, max1, submergeSortSimple);
	mergeSort(array, min2, max2, submergeSortSimple);
}

void submergeSortProc(int* array, int min1, int max1, int min2, int max2){
	int stat_loc, sem_val;
	pid_t r, l;
	sem_t* sem_ref=sem_open("/Semafor",O_RDWR|O_CREAT|O_EXCL,0660,1);
	//for(int i=0; i<N; i++)
	switch(r=fork()){
	case -1:
		break;
	case 0:
		//sem_wait(sem_ref);
		mergeSort(array,min2,max2,submergeSortProc);
		_exit(NULL);
		sem_post(sem_ref);
		break;
	default:
		switch(l=fork()){
		case -1:
			break;
		case 0:
			//sem_wait(sem_ref);
			mergeSort(array,min1,max1,submergeSortProc);
			_exit(NULL);
			sem_post(sem_ref);
			break;
		}
	}
	waitpid(r, &stat_loc, NULL);
	waitpid(l, &stat_loc, NULL);
	merge(array, min1, max1, max2);
}

pthread_mutex_t mx;

struct{
	int *tabela;
	int zacetek;
	int konec;
}typedef thread_args;

void thread_fun(void* args){
	thread_args *thr_arg=(thread_args*)args;
	//mutex lock
	mergeSort(thr_arg->tabela, thr_arg->zacetek, thr_arg->konec, submergeSortThread);
	//mutex unlock
	return 0;
}

void submergeSortThread(int* array, int min1, int max1, int min2, int max2){
	pthread_t tid[2]; //pthread_t tid[N]
	thread_args arg[2]; //thread_args arg[N]

	arg[0].tabela=array; // for(int i=0; i<N; i++){
	arg[0].zacetek=min1; // 	arg[N].tabela=array;
	arg[0].konec=max1;   //		arg[N].zacetek=N*min...

	arg[1].tabela=array; //min1,2,3,4...m|=maxN/N+....
	arg[1].zacetek=min2;
	arg[1].konec=max2;

	pthread_mutex_lock(&mx);
	pthread_create(&tid[0],NULL,&thread_fun, &arg[0]);
	pthread_mutex_unlock(&mx);

	pthread_mutex_lock(&mx);
	pthread_create(&tid[1],NULL,&thread_fun, &arg[1]);
	pthread_mutex_unlock(&mx);

	pthread_join(tid[0], NULL); //for
	pthread_join(tid[1], NULL);

	return;
}

void mergeSort(int *array, int min, int max, void(*submergeSort)(int *, int, int, int, int) ){
	int mid;
	if(min<max){
		mid=(min+max)/2;
		submergeSort(array, min, mid, mid+1, max);
		merge(array, min, mid, max);
	}
}

void merge(int *arr, int min,int mid,int max){
    int *tmp = malloc((max-min+1)*sizeof(int));
    int i,j,k,m;
    j=min;
    m=mid+1;
    for(i=min; j<=mid && m<=max; i++){
        if(arr[j]<=arr[m]){
            tmp[i-min]=arr[j];
            j++;
        }else{
            tmp[i-min]=arr[m];
            m++;
        }
    }
    if(j>mid){
        for(k=m; k<=max; k++){
            tmp[i-min]=arr[k];
            i++;
        }
    }else{
        for(k=j; k<=mid; k++){
            tmp[i-min]=arr[k];
            i++;
        }
    }
    for(k=min; k<=max; k++) arr[k]=tmp[k-min];
    free(tmp);
}

int main(int argc, char *argv[]) {

    //if(argc==5) N=atoi(argv[2]);

    #define NO_PAR 0
    #define PROC_PAR 1
    #define THREAD_PAR 2
    int technique= NO_PAR;
    void (*submergeSortFun)(int *, int, int, int, int);
    submergeSortFun = submergeSortSimple;
    while(1){
        int c;
        c = getopt(argc, argv, "pt");
        if(c==-1) break;
        switch(c){
            case 'p':
                technique = PROC_PAR;
                submergeSortFun = submergeSortProc;
                break;
            case 't':
                technique = THREAD_PAR;
                submergeSortFun = submergeSortThread;
                break;
            default:
                printHelp(argc, argv);
                return 0;
        }
    }
    int i;
    int size;
    int *arr;
    if(optind >= argc){
        printHelp(argc, argv);
        return -1;
    }
    size = atoi(argv[optind]);

    switch(technique){
        case NO_PAR:
            arr = malloc(sizeof(int)*size);
            break;
        case PROC_PAR:
            arr = mmap(0,sizeof(int)*size,PROT_READ|PROT_WRITE,MAP_SHARED|MAP_ANONYMOUS,-1,0);
	    sem_t* sem_ref=sem_open("/Semafor",O_RDWR|O_CREAT|O_EXCL,0660,1);
            break;
        case THREAD_PAR:
	    arr = mmap(0,sizeof(int)*size,PROT_READ|PROT_WRITE,MAP_SHARED|MAP_ANONYMOUS,-1,0);
	    pthread_mutex_init(&mx,NULL);
            break;
    }
    char buffer[101];
    for(i=0; i < size; i+=1) read(0, &arr[i], 4);
    for(i=0; i<size; i++) printf("%d ",arr[i]);
    printf("\n");
    mergeSort(arr, 0, size-1, submergeSortFun);
    for(i=0; i<size; i++) printf("%d ",arr[i]);
    printf("\n");

    switch(technique){
        case NO_PAR:
            free(arr);
            break;
        case PROC_PAR:
	    munmap(0,sizeof(int)*size);
	    sem_unlink("/Semafor");
	    break;
        case THREAD_PAR:
	    munmap(0,sizeof(int)*size);
	    pthread_mutex_destroy(&mx);
            break;
    }
    return 0;
}

void printHelp(int argc, char** argv){
    printf("uporaba\n");
    printf("%s <opcija> <n>\n",argv[0]);
    printf("\tn je število celih števil prebranih z standardnega vhoda\n");
    printf("\tfunkcije prebere n*4 bajtov v tabelo in jih sortira\n");
    printf("opcije:\n");
    printf("-p\n");
    printf("\tparalelizacija s pomočjo procesov\n");
    printf("-t\n");
    printf("\tparalelizacija s pomočjo niti\n");
}
