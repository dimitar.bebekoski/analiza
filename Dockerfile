# Navajan sem na to distribucijo
FROM ubuntu:16.04

# posodobi sistem ter prenesi potrebni orodja od oficijalnih aptitude repozitorijih
RUN apt-get update && apt-get upgrade -y && apt-get -y install pylint valgrind
